import argparse
import sys
from random import randint
from time import sleep
from resources import RELATIVE_PATH,URL,USER,PASS
from selenium import webdriver
import os 
import time
from selenium.webdriver.chrome.options import Options

option = Options()

option.add_argument("--disable-infobars")
option.add_argument("start-maximized")
option.add_argument("--disable-extensions")

# Pass the argument 1 to allow and 2 to block
option.add_experimental_option("prefs", { 
    "profile.default_content_setting_values.notifications": 2 # khong cho phep nhan thong bao
})
FB_URL = "https://fb.com"
 
dirname = os.path.join(__file__)
PATH = os.path.join(dirname,RELATIVE_PATH)
def random_sleep(min_s, max_s):
    sleep(randint(min_s, max_s))
 
 
class FacebookLogin:
    def __init__(self, username, password):
        self.username = username
        self.password = password
        self.driver = webdriver.Chrome(chrome_options=option, executable_path=PATH)
 
    def login(self):
        self.driver.get(FB_URL)
        username_ele = self.driver.find_element_by_css_selector('#email')
        username_ele.send_keys(self.username)
        random_sleep(1, 5)
        password_ele = self.driver.find_element_by_css_selector('#pass')
        password_ele.send_keys(self.password)
        random_sleep(1, 5)
        login_ele = self.driver.find_element_by_css_selector('#loginbutton > input[type="submit"]')
        random_sleep(1, 5)
        login_ele.click()
 
    def verify_login(self):
        try:
            self.driver.find_element_by_css_selector('#email')
            return False
        except:
            return True
 
 
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Auto FB login')
    parser.add_argument('--username', default=None, required=True, help='FB username')
    parser.add_argument('--password', default=None, required=True, help='FB password')
 
    try:
        options = parser.parse_args()
    except:
        parser.print_help()
        sys.exit(0)
 
    fb = FacebookLogin(options.username, options.password)
    fb.login()
    if fb.verify_login():
        print('Đăng nhập thành công!')
    else:
        print('Đăng nhập thất bại')
    # time.sleep(10)
    fb.driver.execute_script(open("./js/infinityScroll.js").read())
    print("scroll successfully!")
    # fb.driver.close()
